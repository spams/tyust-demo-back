package com.example.mybatisplus.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.model.domain.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplus.model.dto.PageDTO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljh
 * @since 2023-01-03
 */
public interface RoleService extends IService<Role> {
    Role login(Role role);

    Page<Role> pageList(PageDTO pageDTO, Role role);

}
