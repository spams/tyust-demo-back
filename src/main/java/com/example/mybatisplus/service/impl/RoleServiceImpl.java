package com.example.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.model.domain.Role;
import com.example.mybatisplus.mapper.Role_Mapper;
import com.example.mybatisplus.model.dto.PageDTO;
import com.example.mybatisplus.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljh
 * @since 2023-01-03
 */
@Service
public class RoleServiceImpl extends ServiceImpl<Role_Mapper, Role> implements RoleService {
    @Autowired
    private Role_Mapper roleMapper;

    @Override
    public Role login(Role role) {
        QueryWrapper<Role> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.eq("sn", role.get角色ID()).eq("password", role.get角色名称())
                .eq("is_enabled", 1);
        Role one = roleMapper.selectOne(roleQueryWrapper);
        return one;
    }

    @Override
    public Page<Role> pageList(PageDTO pageDTO, Role role) {
        Page<Role>page =new Page<>(pageDTO.getPageNo(),pageDTO.getPageSize());
        QueryWrapper<Role>wrapper=new QueryWrapper<>();
        if(StringUtils.isNoneBlank()){
            wrapper.like("sn", role.get角色ID());
        }
        if(StringUtils.isNoneBlank(role.get角色名称())){
            wrapper.like("password", role.get角色名称());
        }
        if(role.get角色ID() != null){
            wrapper.eq("role_id", role.get角色ID());
        }
        roleMapper.selectPage(page,wrapper);
        return page;
    }

}
