package com.example.mybatisplus.mapper;

import com.example.mybatisplus.model.domain.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljh
 * @since 2023-01-03
 */
public interface RoleMapper extends BaseMapper<Role> {

}
