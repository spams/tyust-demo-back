package com.example.mybatisplus.mapper;

import com.example.mybatisplus.model.domain.SuperManagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxq
 * @since 2023-01-03
 */
public interface SuperManagementMapper extends BaseMapper<SuperManagement> {

}
