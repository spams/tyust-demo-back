package com.example.mybatisplus.mapper;

import com.example.mybatisplus.model.domain.SysAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxq
 * @since 2023-01-02
 */
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

}
