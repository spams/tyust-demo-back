package com.example.mybatisplus.model.dto;

import lombok.Data;

    @Data
public class AdminDTO {
        private Long id;
        private String name;
        private Long userType;

}
