package com.example.mybatisplus.model.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxq
 * @since 2023-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysAdmin对象", description="")
public class SysAdmin extends Model<SysAdmin> {

    private static final long serialVersionUID = 1L;

    private Long adminId;

    @TableId("admin_sn")
    private String adminSn;

    private String adminName;

    private String password;

    private Long roleId;

    private String roleName;


    @Override
    protected Serializable pkVal() {
        return this.adminSn;
    }

}
