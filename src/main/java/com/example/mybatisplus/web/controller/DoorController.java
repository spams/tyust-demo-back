package com.example.mybatisplus.web.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.model.domain.User;
import com.example.mybatisplus.model.dto.PageDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.mybatisplus.common.JsonResponse;
import com.example.mybatisplus.service.DoorService;
import com.example.mybatisplus.model.domain.Door;


/**
 *
 *  前端控制器
 *
 *
 * @author lxq
 * @since 2023-01-04
 * @version v1.0
 */
@Controller
@RequestMapping("/api/door")
public class DoorController {
    @Autowired
    private DoorService doorService;
    @GetMapping("/list")
    @ResponseBody
    public  JsonResponse list(PageDTO pageDTO , Door door){
        Page<Door> page =doorService.pageList(pageDTO,door);
        return JsonResponse.success(page);
    }
    @PostMapping("update")
    @ResponseBody
    public JsonResponse update(@RequestBody Door door){
        boolean b = doorService.updateById(door);
        return JsonResponse.success(b);
    }

}

