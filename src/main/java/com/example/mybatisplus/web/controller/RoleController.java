package com.example.mybatisplus.web.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.common.utls.SecurityUtils;
import com.example.mybatisplus.model.dto.DeleteDTO;
import com.example.mybatisplus.model.dto.PageDTO;
import com.example.mybatisplus.model.dto.RoleInfoDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.mybatisplus.common.JsonResponse;
import com.example.mybatisplus.service.RoleService;
import com.example.mybatisplus.model.domain.Role;


/**
 *
 *  前端控制器
 *
 *
 * @author ljh
 * @since 2023-01-03
 * @version v1.0
 */
@Controller
@RequestMapping("/api/role")
public class RoleController {
    @GetMapping("/RoleInfo")
    public JsonResponse<RoleInfoDTO> getRoleInfo() {
        return JsonResponse.success(SecurityUtils.getUserInfo());
    }
    @Autowired
    private RoleService roleService;
    @PostMapping("/login")
    @ResponseBody
    public  JsonResponse login(@RequestBody Role role){
        Role login = roleService.login(role);

        return JsonResponse.success(login);
    }
    @GetMapping("st")
    @ResponseBody
    public  JsonResponse list(PageDTO pageDTO , Role Role){
        Page<Role> page =roleService.pageList(pageDTO,Role);
        return JsonResponse.success(page);
    }
    //根据ID删除
    @GetMapping("removeById")
    @ResponseBody
    public JsonResponse removeById(Long  id){
        boolean b = roleService.removeById(id);
        return JsonResponse.success(b);
    }
    //根据ID批量删除
    @PostMapping("removeByIds")
    @ResponseBody
    public JsonResponse removeByIds(@RequestBody DeleteDTO deleteDTO){
        boolean b = roleService.removeByIds(deleteDTO.getIds());
        return JsonResponse.success(b);
    }
    //前端修改后端保存
    @PostMapping("update")
    @ResponseBody
    public JsonResponse update(@RequestBody Role role){
        boolean b = roleService.updateById(role);
        return JsonResponse.success(b);
    }

}

