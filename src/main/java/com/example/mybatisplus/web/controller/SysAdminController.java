package com.example.mybatisplus.web.controller;

import com.example.mybatisplus.common.utls.SessionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.mybatisplus.common.JsonResponse;
import com.example.mybatisplus.service.SysAdminService;
import com.example.mybatisplus.model.domain.SysAdmin;


/**
 *
 *  前端控制器
 *
 *
 * @author lxq
 * @since 2023-01-02
 * @version v1.0
 */
@Controller
@RequestMapping("/api/sysAdmin")
public class SysAdminController {
    @Autowired
    private SysAdminService sysAdminService;

    @PostMapping("/login")
    @ResponseBody
    public JsonResponse login(@RequestBody SysAdmin sysAdmin) {
        SysAdmin login = sysAdminService.login(sysAdmin);
        if (login != null) {
            SessionUtils.saveCurrentUser(login);
        }
        return JsonResponse.success(login);
    }
}

