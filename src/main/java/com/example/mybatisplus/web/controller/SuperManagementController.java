package com.example.mybatisplus.web.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.common.utls.SecurityUtils;
import com.example.mybatisplus.model.dto.DeleteDTO;
import com.example.mybatisplus.model.dto.PageDTO;
import com.example.mybatisplus.model.dto.UserInfoDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.mybatisplus.common.JsonResponse;
import com.example.mybatisplus.service.SuperManagementService;
import com.example.mybatisplus.model.domain.SuperManagement;


/**
 *
 *  前端控制器
 *
 *
 * @author lxq
 * @since 2023-01-03
 * @version v1.0
 */
@Controller
@RequestMapping("/api/superManagement")
public class SuperManagementController {

    private final Logger logger = LoggerFactory.getLogger( SuperManagementController.class );

    @Autowired
    private SuperManagementService superManagementService;

    @GetMapping("/SuperManagementInfo")
    public JsonResponse<UserInfoDTO> getSuperManagementInfo() {
        return JsonResponse.success(SecurityUtils.getUserInfo());
    }
    @Autowired
    private SuperManagementService SuperManagementService;
    @PostMapping("/login")
    @ResponseBody
    public  JsonResponse login(@RequestBody SuperManagement SuperManagement){
        SuperManagement login = SuperManagementService.login(SuperManagement);

        return JsonResponse.success(login);
    }
    @GetMapping("st")
    @ResponseBody
    public  JsonResponse list(PageDTO pageDTO , SuperManagement SuperManagement){
        Page<SuperManagement> page =SuperManagementService.pageList(pageDTO,SuperManagement);
        return JsonResponse.success(page);
    }
    //根据ID删除
    @GetMapping("removeById")
    @ResponseBody
    public JsonResponse removeById(Long  id){
        boolean b = SuperManagementService.removeById(id);
        return JsonResponse.success(b);
    }
    //根据ID批量删除
    @PostMapping("removeByIds")
    @ResponseBody
    public JsonResponse removeByIds(@RequestBody DeleteDTO deleteDTO){
        boolean b = SuperManagementService.removeByIds(deleteDTO.getIds());
        return JsonResponse.success(b);
    }
    //前端修改后端保存
    @PostMapping("update")
    @ResponseBody
    public JsonResponse update(@RequestBody SuperManagement SuperManagement){
        boolean b = SuperManagementService.updateById(SuperManagement);
        return JsonResponse.success(b);
    }

    /**
    * 描述：根据Id 查询
    *
    */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getById(@PathVariable("id") Long id)throws Exception {
        SuperManagement  superManagement =  superManagementService.getById(id);
        return JsonResponse.success(superManagement);
    }

    /**
    * 描述：根据Id删除
    *
    */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public JsonResponse deleteById(@PathVariable("id") Long id) throws Exception {
        superManagementService.removeById(id);
        return JsonResponse.success(null);
    }


    /**
    * 描述：根据Id 更新
    *
    */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public JsonResponse updateSuperManagement(SuperManagement  superManagement) throws Exception {
        superManagementService.updateById(superManagement);
        return JsonResponse.success(null);
    }


    /**
    * 描述:创建SuperManagement
    *
    */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse create(SuperManagement  superManagement) throws Exception {
        superManagementService.save(superManagement);
        return JsonResponse.success(null);
    }
}

